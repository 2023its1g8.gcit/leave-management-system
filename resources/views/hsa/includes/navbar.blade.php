<div class="header">
		<div class="header-left">
			<div class="menu-icon dw dw-menu"></div>
			<div class="search-toggle-icon dw dw-search2" data-toggle="header_search"></div>
			
		</div>
		<div class="header-right">
			<div class="dashboard-setting user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="javascript:;" data-toggle="right-sidebar">
						<i class="dw dw-settings2"></i>
					</a>
				</div>
			</div>
			
			<div class="user-info-dropdown">
				<div class="dropdown">


					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon" style="width: 40px;height:40px;">
							@if (Auth::user()->profileImg)
							<img style="width: 100%;height:100%;object-fit:cover;object-position:center;" src="{{ asset(Auth::user()->profileImg) }}" class="avatar-photo" alt="Profile Image">
							@else
							<div class="profileContainer">
								<div class="text-uppercase" style="font-weight: 500;font-size:14px;">{{ implode(' ', array_map(function($part) { return strtoupper(substr($part, 0, 1)); }, explode(' ', Auth::user()->name))) }}</div>
							</div>	
							@endif
						</span>
						<span class="user-name">{{Auth::user()->name }}</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<a class="dropdown-item" href="profile"><i class="dw dw-user1"></i> Profile</a>
						<a class="dropdown-item" href="changePass"><i class="dw dw-help"></i> Change Password</a>
						<form method="POST" class="dropdown-item" action="{{ route('logout') }}">
							@csrf
							<a href="{{ route('logout') }}" onclick="event.preventDefault();
								this.closest('form').submit();"><i class="dw dw-logout"></i> Log Out</a>
						</form>
					</div>
				</div>
			</div>
			
		</div>
				{{-- Messages --}}
				@if(session('success'))
				<div class="sucesssMessage pb-3" id="HSAsuccessMessage">
					{{ session('success') }}
				</div>
				@endif
		
				@if(session('error'))
				<div class="errorMessage pb-3" id="HSAerrorMessage">
					{{ session('error') }}
				</div>
				@endif
			{{-- Display Validation Errors --}}
				@if ($errors->any())
				<div class="errorMessage pb-3">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<script>
					// Hide success message after 3 seconds
					var successMessage = document.getElementById('HSAsuccessMessage');
					if (successMessage) {
						setTimeout(function () {
							successMessage.style.display = 'none';
						}, 3000); // 3000 milliseconds = 3 seconds
					}
				
					// Hide error message after 3 seconds
					var errorMessage = document.getElementById('HSAerrorMessage');
					if (errorMessage) {
						setTimeout(function () {
							errorMessage.style.display = 'none';
						}, 7000); // 3000 milliseconds = 3 seconds
					}
				</script>
	</div>