<div class="left-side-bar">
		<div class="brand-logo">
			<a href="">
				<img src="../vendors/images/logo.png" alt="" class="dark-logo">
				<span style="color: grey; padding-left: 5px;">SLMS</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li class="dropdown">
						<a href="dashboard" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-house-1"></span><span class="mtext">Dashboard</span>
						</a>
						
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-library"></span><span class="mtext">SSO</span>
						</a>
						<ul class="submenu">
							<li><a href="addSso">Add SSO</a></li>
							<li><a href="mgSso">Manage SSO</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon dw dw-apartment"></span><span class="mtext"> Leave </span>
						</a>
						<ul class="submenu">
							<li><a href="applyLeave">Apply Leave</a></li>
							<li><a href="allLeaves">All</a></li>
							<li><a href="PendingLeaves">Pending</a></li>
							<li><a href="ApprovedLeaves">Approved</a></li>
							<li><a href="RejectedLeaves">Rejected</a></li>
						</ul>
					</li>

					<!-- <li>
						<div class="dropdown-divider"></div>
					</li> -->
				</ul>
			</div>
		</div>
	</div>