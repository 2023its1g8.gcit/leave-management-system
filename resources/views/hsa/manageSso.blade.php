@include('hsa.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

	@include('hsa.includes.navbar')

	@include('hsa.includes.right_sidebar')

	@include('hsa.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">

		<div class="pd-ltr-20">	

			<div class="page-header">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="title">
							<h4>Manage Student</h4>
						</div>
						<nav aria-label="breadcrumb" role="navigation">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
								<li class="breadcrumb-item active" aria-current="page">Manage Student</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>

			<div class="card-box mb-30">
				<div class="pd-20">
						<h2 class="text-blue h4">SSOs</h2>
					</div>
				<div class="pb-20">
					<table class="data-table table stripe hover nowrap">
						<thead>
							<tr>
								<th class="table-plus">FULL NAME</th>
								<th>EMPLOYEE NO.</th>
								<th>EMAIL ADDRESS</th>
								<th>GENDER</th>
								<th>CONTACT NO.</th>
								<th class="datatable-nosort">ACTION</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($ssoUser  as $s)
							<tr>
								<td class="table-plus">
									<div class="name-avatar d-flex align-items-center">
										<div class="avatar mr-2 flex-shrink-0">
											@if ($s->profileImg)
											<img style="width: 40px;height:40px;object-fit:cover;object-position:center;border-radius: 100%;border: 1px solid #000;" src="{{ asset($s->profileImg) }}" class="avatar-photo" alt="Profile Image">
											@else
											<div class="profileContainer">
												<div class="text-uppercase">{{ implode(' ', array_map(function($part) { return strtoupper(substr($part, 0, 1)); }, explode(' ', $s->name))) }}</div>
											</div>
											@endif
								
										</div>
										<div class="txt pl-3">
											<div class="weight-600">{{$s->name}}</div>
										</div>
									</div>
								</td>
								<td>{{$s->id}}</td>
	                            <td>{{$s->email}}</td>
								<td>{{$s->gender}}</td>
								<td>{{$s->contact_number}}</td>
								<td>
									<div class="dropdown">
										<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"  href="#" role="button" data-toggle="dropdown">
											<i class="dw dw-more"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
											<button class="dropdown-item"  data-toggle="modal" data-target="#editStudent{{$s->id}}"><i class="dw dw-edit2"></i> Edit</button>
											<button data-toggle="modal" data-target="#deleteSSO{{$s->id}}" class="dropdown-item"><i class="dw dw-delete-3"></i> Delete</button>
										</div>
									</div>
								</td>
							</tr>
						<!-- Modal -->
						<div class="modal fade" id="deleteSSO{{$s->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteSSOTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLongTitle">DELETE SSO</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>
								<div class="modal-body">
								Are you sure do you want to delete this sso, <span style="color: red">{{$s->name}}</span>
								</div>
								<div class="modal-footer justify-content-center">
									<form action="{{url('hsa/deleteSSO')}}" method="POST">
										@csrf
										<input name="user_id" hidden type="text" value="{{$s->id}}"/>
										<button type="submit" style="width: 150px" type="button" class="btn btn-primary bg-danger">YES</button>
									</form>
									<button style="width: 150px" type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
								</div>
							</div>
							</div>
						</div>	
						
						
						<!-- Edit SSO Modal -->
							<div class="modal fade" id="editStudent{{$s->id}}" tabindex="-1" role="dialog" aria-labelledby="editStudentLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content p-4" style="border-radius: 20px;">
									<div class="modal-header">
									<h5 class="modal-title" id="editStudentLabel">Edit SSO: {{$s->name}}</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>
									<div class="modal-body">
										<div class="wizard-content">
											<form class="mt-3" method="post" action="{{url('hsa/editSSO')}}">
												@csrf
												<input hidden  type="number" name="id" value="{{$s->id}}">
												<input hidden  type="text" name="role" value="{{$s->role}}">
												<section>
													<div class="row">
														<div class="col-md-6 col-sm-12">
															<div class="form-group">
																<label>Full Name :</label>
																<input name="name" type="text" class="form-control wizard-required" value="{{$s->name}}" required="true" autocomplete="off">
															</div>
														</div>
														<div class="col-md-6 col-sm-12">
															<div class="form-group">
																<label>Email Address :</label>
																<input name="email"  value="{{$s->email}}" type="email" class="form-control" required="true" autocomplete="off">
															</div>
														</div>
													</div>
				
													<div class="row">
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Employee ID :</label>
																<input name="employeID" type="number" class="form-control" required="true" value="{{$s->id}}" autocomplete="off">
															</div>
														</div>
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Phone Number :</label>
																<input name="phoneNumber" type="number" class="form-control" required="true" value="{{$s->contact_number}}" autocomplete="off">

															</div>
														</div>
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Gender :</label>
																<select name="gender" class="custom-select form-control" required="true" autocomplete="off">
																	<option value="{{$s->gender}}">{{$s->gender}}</option>
																	<option value="male">Male</option>
																	<option value="female">Female</option>
																</select>																
															</div>
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-12 col-sm-12">
															<div class="form-group">
																<label style="font-size:16px;"><b></b></label>
																<div class="modal-footer justify-content-start">
																	<button type="submit" class="btn w-100 btn-primary" name="add_staff" id="add_staff" data-toggle="modal">Save The Changes</button>
																</div>
															</div>
														</div>
													</div>
												</section>
											</form>
										</div>
									</div>
								</div>
								</div>
							</div>
							@endforeach
						</tbody>
					</table>


  


			   </div>
			</div>
			@include('hsa.includes.footer')

		</div>
	</div>
	
	@include('hsa.includes.scripts')

</body>
</html>