@include('sso.includes.header')
<style>
    .login-page{
        background-color: red;
        background: url('{{ asset('vendors/images/Group 5.png') }}');
        width: 100%;
        min-height: 100vh;
        background-repeat: no-repeat;
        background-position: bottom;
        background-size: cover;
    }
</style>
<body class="login-page">
    {{-- <div class="login-header box-shadow">
        <div class="container-fluid d-flex justify-content-between align-items-center">
            <div class="brand-logo">
                <a  href="{{ route('login') }}">
                    <img src="{{ asset('vendors/images/logo.png') }}" alt="">
                </a>
            </div>
        </div>
    </div> --}}
    <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-lg-7">
                    {{-- <img src="{{ asset('vendors/images/bg.png') }}" alt=""> --}}
                </div>
                <div class="col-md-6 col-lg-5 bg-white box-shadow border-radius-10 pb-5" style="box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;">
                    <div class="login-box ">
                        <div class="login-title">
                            <h2 class="text-center text-primary">Welcome To SLMS</h2>
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <!-- Email Address -->
                            <div class="input-group custom">
                                <input type="email" class="form-control form-control-lg" placeholder="Email ID" name="email" id="email" :value="old('email')" required autofocus autocomplete="username">
                                <div class="input-group-append custom">
                                    <span class="input-group-text"><i class="icon-copy fa fa-envelope-o" aria-hidden="true"></i></span>
                                </div>
                            </div>

                            <!-- Password -->
                            <div class="input-group custom">
                                <input type="password" class="form-control form-control-lg" placeholder="**********" name="password" id="password" required autocomplete="current-password">
                                <div class="input-group-append custom">
                                    <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                                </div>
                            </div>
                            <div class="input-group custom">
                                <x-input-error :messages="$errors->get('email')" class="mt-1" style="color: red"/>
                            </div>

                            <!-- Forgot Password -->
                            <div class="row pb-30">
                                <div class="col-6">
                                    <div class="forgot-password"><a href="{{ route('password.request') }}" style="color: blue">Forgot Password</a></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-group mb-0">
                                       <input class="btn btn-primary btn-lg btn-block" name="signin" id="signin" type="submit" value="Sign In">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- js -->
    <script src="{{ asset('vendors/scripts/core.js') }}"></script>
    <script src="{{ asset('vendors/scripts/script.min.js') }}"></script>
    <script src="{{ asset('vendors/scripts/process.js') }}"></script>
    <script src="{{ asset('vendors/scripts/layout-settings.js') }}"></script>
</body>
</html>
