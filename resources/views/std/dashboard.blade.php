@include('std.includes.header')
<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/logo2.png" width="100" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}
	
	@include('std.includes.navbar')

	@include('std.includes.right_sidebar')

	@include('std.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20">
			<div class="card-box pd-20 height-100-p mb-30">
				<div class="row align-items-center">
					<div class="col-md-3 user-icon">
						@if (Auth::user()->profileImg)
						<img style="width: 200px;height:200px;object-fit:cover;object-position:center;border-radius:15px;box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;" src="{{ asset(Auth::user()->profileImg) }}" class="avatar-photo" alt="Profile Image">
						@else
						<div style="width: 200px;height:200px;object-fit:cover;object-position:center;border-radius:15px;box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;" class="profileContainer">
							<div  class="text-uppercase" style="font-size: 60px" >{{ implode(' ', array_map(function($part) { return strtoupper(substr($part, 0, 1)); }, explode(' ', Auth::user()->name))) }}</div>
						</div>
						@endif
					</div>
					<div class="col-md-8">

						<h4 class="font-20 weight-500 mb-10 text-capitalize">
							Welcome back <br> <div class="weight-600 font-30 text-blue">{{ Auth::user()->name }} !</div>
						</h4>
						<p class="font-18 max-width-600">you are in an institution of GCIT.</p>
					</div>
				</div>
			</div>

			<div class="title pb-20">
				<h2 class="h3 mb-0 text-uppercase">Leave Overview</h2>
			</div>
			
			<div class="row pb-10">
				<div class="col-xl-6 col-lg-3 col-md-6 mb-20">
					<div class="card-box height-100-p widget-style3">

						<div class="d-flex flex-wrap">
							<div class="widget-data">
								<div class="weight-700 font-24 text-dark">{{$leaveAppliedCount}}</div>
								<div class="font-14 text-secondary weight-500">All Leaves</div>
							</div>
							<div class="widget-icon">
								<div class="icon" data-color="#00eccf"><i class="icon-copy dw dw-user-2"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-3 col-md-6 mb-20">
					<div class="card-box height-100-p widget-style3">

						<div class="d-flex flex-wrap">
							<div class="widget-data">
								<div class="weight-700 font-24 text-dark">{{$approveLeaveCount}}</div>
								<div class="font-14 text-secondary weight-500">Approved</div>
							</div>
							<div class="widget-icon">
								<div class="icon" data-color="#09cc06"><span class="icon-copy fa fa-hourglass"></span></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-3 col-md-6 mb-20">
					<div class="card-box height-100-p widget-style3">

						<div class="d-flex flex-wrap">
							<div class="widget-data">
								<div class="weight-700 font-24 text-dark">{{$pendingLeaveCount}}</div>
								<div class="font-14 text-secondary weight-500">Pending</div>
							</div>
							<div class="widget-icon">
								<div class="icon"><i class="icon-copy fa fa-hourglass-end" aria-hidden="true"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-3 col-md-6 mb-20">
					<div class="card-box height-100-p widget-style3">

						<div class="d-flex flex-wrap">
							<div class="widget-data">
								<div class="weight-700 font-24 text-dark">{{$declineLeaveCount}}</div>
								<div class="font-14 text-secondary weight-500">Rejected</div>
							</div>
							<div class="widget-icon">
								<div class="icon" data-color="#ff5b5b"><i class="icon-copy fa fa-hourglass-o" aria-hidden="true"></i></div>
							</div>
						</div>
					</div>
				</div>
			</div>


			{{-- @include('std.includes.footer') --}}
		</div>
	</div>
	<!-- js -->

	@include('std.includes.scripts')
</body>
</html>