@include('std.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

	@include('std.includes.navbar')

	@include('std.includes.right_sidebar')

	@include('std.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="title">
								<h4>PROFILE</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
									<li class="breadcrumb-item active" aria-current="page">Profile</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-30">
						<div class="pd-20 card-box height-100-p">

							<div class="profile-photo">
								<a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar"><i class="fa fa-pencil"></i></a>
								@if (Auth::user()->profileImg)
									<img style="width: 150px;height:150px;object-fit:cover;object-position:center;" src="{{ asset(Auth::user()->profileImg) }}" class="avatar-photo" alt="Profile Image">
								@else
								<div style="width: 150px;height:150px;object-fit:cover;object-position:center;" class="profileContainer">
									<div style="font-size: 40px;" class="text-uppercase" style="font-weight: 500;font-size:14px;">{{ implode(' ', array_map(function($part) { return strtoupper(substr($part, 0, 1)); }, explode(' ', Auth::user()->name))) }}</div>
								</div>								
								@endif
									<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">		
											<form action="{{ route('profile.updateProfileImage') }}" method="POST" enctype="multipart/form-data">
												@csrf
												@method('PUT')

												<div class="weight-500 col-md-12 pd-5">
															<div class="form-group">
																<div class="custom-file">
																	<input name="profileImg" id="file" type="file" class="custom-file-input" accept="image/*" onchange="validateImage('file')">
																	<label class="custom-file-label" for="file" id="selector">Choose file</label>		
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<input type="submit" name="update_image" value="Update" class="btn btn-primary">
															<button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
														</div>
												</form>
											</div>
										</div>
									</div>
							</div>
							<div class="profile-info  text-center">
								<h5 class="mb-20 h5 text-blue text-uppercase">Contact Information</h5>
								<ul>
                                    <li style="display: flex;justify-content:center;gap:10px;">
										<span>Name:</span> {{Auth::user()->name}}
									</li>
                                    <li style="display: flex;justify-content:center;gap:10px;">
										<span>Employee Number:</span>{{Auth::user()->id}}
									</li>
									<li style="display: flex;justify-content:center;gap:10px;" >
										<span>Email:</span>{{Auth::user()->email}}
									</li>
									<li style="display: flex;justify-content:center;gap:10px;">
										<span>My Role:</span> 
										@if (Auth::user()->role === 'std')
											Student
								
										@else
												{{Auth::user()->role}}
										@endif
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			{{-- @include('sso.includes.footer') --}}
		</div>
	</div>
	
    @include('sso.includes.scripts')

	<script type="text/javascript">
		var loader = function(e) {
			let file = e.target.files;

			let show = "<span>Selected file : </span>" + file[0].name;
			let output = document.getElementById("selector");
			output.innerHTML = show;
			output.classList.add("active");
		};

		let fileInput = document.getElementById("file");
		fileInput.addEventListener("change", loader);
	</script>
	<script type="text/javascript">
		 function validateImage(id) {
		    var formData = new FormData();
		    var file = document.getElementById(id).files[0];
		    formData.append("Filedata", file);
		    var t = file.type.split('/').pop().toLowerCase();
		    if (t != "jpeg" && t != "jpg" && t != "png") {
		        alert('Please select a valid image file');
		        document.getElementById(id).value = '';
		        return false;
		    }
		    if (file.size > 1050000) {
		        alert('Max Upload size is 1MB only');
		        document.getElementById(id).value = '';
		        return false;
		    }

		    return true;
		}
	</script>
</body>
</html>