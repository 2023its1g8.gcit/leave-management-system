@include('std.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

	@include('std.includes/navbar')

	@include('std.includes/right_sidebar')

	@include('std.includes/left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20">
			<div class="page-header">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="title">
							<h4>LEAVE HISTORY</h4>
						</div>
						<nav aria-label="breadcrumb" role="navigation">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
								<li class="breadcrumb-item active" aria-current="page">Leave History</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
			<div class="card-box mb-30">
				<div class="pd-20">
						<h2 class="text-blue h4">LEAVE HISTORY</h2>
					</div>
				<div class="pb-20">
					<table class="data-table table stripe hover nowrap">
						<thead>
							<tr>
								<th class="table-plus">LEAVE TYPE</th>
								<th>DATE FROM</th>
								<th>DATE TO</th>
								<th>NO. OF DAYS</th>
								<th>SSO STATUS</th>
								<th>HSA STATUS</th>
								<th class="datatable-nosort">ACTION</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($leaveRecord as $leave)
			
							<tr>
								  <td  class="text-capitalize">{{$leave->leave_type}}</td>
                                  <td>{{$leave->start_date}}</td>
                                  <td>{{$leave->end_date}}</td>
								  <td>{{ \Carbon\Carbon::parse($leave->start_date)->diffInDays(\Carbon\Carbon::parse($leave->end_date)) }}</td>
                                  <td>
									
									@if ($leave->sso_status === null)
									<span style="color: blue">Pending</span>
									@else
									<span  class="text-capitalize" style="color: {{ $leave->sso_status === 'approve' ? 'green' : ($leave->sso_status === 'decline' ? 'red' : 'black') }}">
										{{ $leave->sso_status }}
									  </span>  
									@endif
								                                 
									</td>
                                    <td>
										@if ($leave->hsa_status === null)
										<span  style="color: blue" >Pending</span>
										@else
											<span class="text-capitalize" style="color: {{ $leave->hsa_status === 'approve' ? 'green' : ($leave->hsa_status === 'decline' ? 'red' : 'black') }}">{{ $leave->hsa_status }}</span>
										@endif
	  
                                    </td>
								   <td>
									  <div class="table-actions">
										<button type="button" class="btn" data-toggle="modal" data-target="#viewLeave{{$leave->id}}">
											<i class="icon-copy dw dw-eye"></i>
										</button>
									  </div>
								   </td>
							</tr>
							<!--View Leave Modal -->
							<div class="modal fade" id="viewLeave{{$leave->id}}" tabindex="-1" aria-labelledby="viewLeaveLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
								<div class="modal-content py-3 px-5" style="border-radius: 20px">
									<div class="modal-header">
									<h5 class="modal-title" id="viewLeaveLabel">Leave Type: <span class="text-capitalize"> {{$leave->leave_type}}</span></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>
									<div class="modal-body">
										<form>

											<div class="row">
												<div class="col-md-12 col-sm-12">
													<div class="form-group">
														<label style="font-size:16px;">Leave Type :</label>
														<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="{{$leave->leave_type}}">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-sm-12">
													<div class="form-group">
														<label style="font-size:16px;">Start Leave Date :</label>
														<input id="date_form" name="date_from" type="date" class="form-control" required="true" autocomplete="off" readonly value="{{$leave->start_date}}">
													</div>
												</div>
												<div class="col-md-6 col-sm-12">
													<div class="form-group">
														<label style="font-size:16px;">End Leave Date :</label>
														<input id="date_to" name="date_to" type="date" class="form-control" required="true" autocomplete="off" readonly value="{{$leave->end_date}}">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 col-sm-12">
													<div class="form-group">
														<label style="font-size:16px;">Reason :</label>
														<input id="date_to" name="reason" type="Textarea" class="form-control" required="true" autocomplete="off" readonly value="{{$leave->reason}}">
													</div>
												</div>
											</div>


											<div class="form-group row">
												<div class="col-md-6 col-sm-12">
													<div class="form-group">
														<label style="font-size:16px;"><b>Date For SSO's Action</b></label>
														<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly  value="{{$leave->sso_approval_date !== null ? $leave->sso_approval_date : 'NA'}}"/>
														{{-- <div class="avatar mr-2 flex-shrink-0">
															<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="2023-10-19">
														</div> --}}
														
													</div>
												</div>
												<div class="col-md-6 col-sm-12">
													<div class="form-group">
														<label style="font-size:16px;"><b>Date For HSA's Action</b></label>
														{{-- <input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="NA"> --}}
														<div class="avatar mr-2 flex-shrink-0">
															<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="{{$leave->hsa_approval_date !== null ? $leave->hsa_approval_date : 'NA'}}">
														</div>
														
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label style="font-size:16px;"><b>Leave Status From SSO</b></label>
													
														<input type="text" style="color: {{$leave->sso_status === 'decline' ? 'red' : ($leave->sso_status === 'approve' ? 'green' : 'blue')}}" class="selectpicker text-capitalize form-control" data-style="btn-outline-primary" readonly value="{{$leave->sso_status !== null ? $leave->sso_status : 'Pending'}}">
														
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label style="font-size:16px;"><b>Leave Status From HSA</b></label>
													
														<input type="text" style="color: {{$leave->hsa_status === 'decline' ? 'red' : ($leave->hsa_status === 'approve' ? 'green' : 'blue')}}" class="selectpicker text-capitalize form-control" data-style="btn-outline-primary" readonly value="{{$leave->hsa_status !== null ? $leave->hsa_status : 'Pending'}}">
								
													</div>
												</div>

											</div>
										</form>
									</div>
								</div>
								</div>
							</div>
							@endforeach
						</tbody>
					</table>
			   </div>
			</div>

			{{-- @include('std.includes.footer') --}}
		</div>
	</div>
	<!-- js -->

	@include('std.includes.scripts')
</body>
</html>