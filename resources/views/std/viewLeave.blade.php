
@include('std.includes.header')

<style>
	input[type="text"]
	{
	    font-size:16px;
	    color: #0f0d1b;
	    font-family: Verdana, Helvetica;
	}

	.btn-outline:hover {
	  color: #fff;
	  background-color: #524d7d;
	  border-color: #524d7d; 
	}

	textarea { 
		font-size:16px;
	    color: #0f0d1b;
	    font-family: Verdana, Helvetica;
	}

	textarea.text_area{
        height: 8em;
        font-size:16px;
	    color: #0f0d1b;
	    font-family: Verdana, Helvetica;
      }

</style>

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

    @include('std.includes.navbar')

    @include('std.includes.right_sidebar')

    @include('std.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>LEAVE DETAILS</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ url('std/dashboard') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url()->previous() }}">Leave History</a></li>
									<li class="breadcrumb-item active" aria-current="page">View Leave</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>

				<div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue h4">Leave Details</h4>
							<p class="mb-20"></p>
						</div>
					</div>
					<form>

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label style="font-size:16px;">Leave Type :</label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="{{$viewLeave->leave_type}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label style="font-size:16px;">Start Leave Date :</label>
                                    <input id="date_form" name="date_from" type="date" class="form-control" required="true" autocomplete="off" readonly value="{{$viewLeave->start_date}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label style="font-size:16px;">End Leave Date :</label>
                                    <input id="date_to" name="date_to" type="date" class="form-control" required="true" autocomplete="off" readonly value="{{$viewLeave->end_date}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label style="font-size:16px;">Reason :</label>
                                    <input id="date_to" name="reason" type="Textarea" class="form-control" required="true" autocomplete="off" readonly value="{{$viewLeave->reason}}">
                                </div>
                            </div>
                        </div>


						<div class="form-group row">
							<div class="col-md-6 col-sm-12">
							    <div class="form-group">
									<label style="font-size:16px;"><b>Date For SSO's Action</b></label>
                                    <input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly  value="{{$viewLeave->sso_approval_date !== null ? $viewLeave->sso_approval_date : 'NA'}}"/>
                                    {{-- <div class="avatar mr-2 flex-shrink-0">
										<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="2023-10-19">
									  </div> --}}
									
							    </div>
							</div>
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Date For HSA's Action</b></label>
                                    {{-- <input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="NA"> --}}
                                    <div class="avatar mr-2 flex-shrink-0">
										<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="{{$viewLeave->hsa_approval_date !== null ? $viewLeave->hsa_approval_date : 'NA'}}">
									  </div>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label style="font-size:16px;"><b>Leave Status From SSO</b></label>
								
									  <input type="text" style="color: {{$viewLeave->sso_status === 'decline' ? 'red' : ($viewLeave->sso_status === 'approve' ? 'green' : 'blue')}}" class="selectpicker text-capitalize form-control" data-style="btn-outline-primary" readonly value="{{$viewLeave->sso_status !== null ? $viewLeave->sso_status : 'Pending'}}">
									
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label style="font-size:16px;"><b>Leave Status From HSA</b></label>
								
									  <input type="text" style="color: {{$viewLeave->hsa_status === 'decline' ? 'red' : ($viewLeave->hsa_status === 'approve' ? 'green' : 'blue')}}" class="selectpicker text-capitalize form-control" data-style="btn-outline-primary" readonly value="{{$viewLeave->hsa_status !== null ? $viewLeave->hsa_status : 'Pending'}}">
			
								</div>
							</div>

						</div>
					</form>
				</div>

			</div>
			
			@include('std.includes.footer'); 
		</div>
	</div>
	<!-- js -->

	@include('std.includes.scripts')
</body>
</html>