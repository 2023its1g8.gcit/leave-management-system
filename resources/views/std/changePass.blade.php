{{-- @include('../includes/session.php') --}}
@include('std.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

    @include('std.includes.navbar')

	@include('std.includes.right_sidebar')

	@include('std.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>CHANGE YOUR PASSWORD</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
									<li class="breadcrumb-item active" aria-current="page">Change Password</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>

				<div class="pd-20 card-box mb-30">
					<div class="wizard-content mt-4">
						<form method="post" action="{{ route('password.update') }}" autocomplete="off"  >
							@csrf
							@method('put')
		
							<section>
								<div class="row justify-content-center">
									<div class="col-md-6 col-sm-12  ">
										<div  class="form-group alert-success d-flex justity-content-center align-item-center ">
											@if (session('status') === 'password-updated')
											<p class="pt-2 pl-3"
												x-data="{ show: true }"
												x-show="show"
												x-transition
												x-init="setTimeout(() => show = false, 2000)"
												class="text-sm text-gray-600">{{ __('password has been succesfully updated!') }}</p>
										@endif 
										</div>
									</div>
							</div>
								<div class="row justify-content-center">
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label>Current Password :</label>
											<input  name="current_password"  type="password" placeholder="**********" class="form-control" required="true" autocomplete="off">
											<x-input-error :messages="$errors->updatePassword->get('current_password')" class="mt-2 alert-danger p-2" />

										</div>
									</div>  
								</div>
                                <div class="row justify-content-center">
                                    <div class="col-md-6 col-sm-12 justify-content-center">
                                        <div class="form-group">
                                            <label>New Password :</label>
                                            <input name="password" type="password" placeholder="**********" class="form-control" required="true" autocomplete="off">
											<x-input-error :messages="$errors->updatePassword->get('password')" class="mt-2 alert-danger p-2" />

                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-6 col-sm-12 justify-content-center">
                                        <div class="form-group">
                                            <label >Confirm Password :</label>
                                            <input  name="password_confirmation" type="password" placeholder="**********" class="form-control" required="true" autocomplete="off">
											<x-input-error :messages="$errors->updatePassword->get('password_confirmation')" class="mt-2 alert-danger p-2 " />

                                        </div>
                                    </div>
                                </div>

								<div class="row justify-content-center">
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label style="font-size:16px;"><b></b></label>
											<div class="modal-footer justify-content-center">
												<button class="btn btn-primary" name="add_staff" id="add_staff" data-toggle="modal">Done</button>
											</div>
										</div>
									</div>
								</div>
							</section>
						</form>
					</div>
				</div>

			</div>
            {{-- @include('std.includes.footer') --}}
		</div>
	</div>
	
	@include('std.includes.scripts')

</body>
</html>