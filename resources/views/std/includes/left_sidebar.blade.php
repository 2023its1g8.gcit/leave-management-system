<div class="left-side-bar">
	<div class="brand-logo">
		<a href="dashboard">
			<img src="../vendors/images/logo.png" alt="" class="dark-logo">
		</a>
		<div class="close-sidebar" data-toggle="left-sidebar-close">
			<i class="ion-close-round"></i>
		</div>
	</div>
	<div class="menu-block customscroll">
		<div class="sidebar-menu">
			<ul id="accordion-menu">
				<li class="dropdown">
					<a href="dashboard" class="dropdown-toggle no-arrow">
						<span class="micon dw dw-house-1"></span><span class="mtext">Dashboard</span>
					</a>
					
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<span class="micon dw dw-apartment"></span><span class="mtext"> Leave </span>
					</a>
					<ul class="submenu">
						<li><a href="applyLeave">Apply Leave</a></li>
						<li><a href="leaveHistory">Leave History</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>