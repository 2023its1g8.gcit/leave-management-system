{{-- @include('../includes/session.php') --}}
@include('sso.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

    @include('sso.includes.navbar')

	@include('sso.includes.right_sidebar')

	@include('sso.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4 class="text-uppercase">Add Student</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
									<li class="breadcrumb-item active mr-auto" aria-current="page">Add Student</li>

								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 d-flex justify-content-end">
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
							Import Excel Data for Students
						</button>
						
						<!-- Modal -->
						<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header pt-4 pl-5 pr-5">
								<h5 class="modal-title" id="exampleModalLabel">Add Students in Bulk</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>
								<div class="modal-body pl-4 pr-4">
								   <form method="POST" action="{{url('sso/importStudentExcel')}}" enctype="multipart/form-data">
									@csrf
									<div class="col-md-12 col-sm-12">
										<div class="form-group">
											<label>Select Excel file (.xlsx):</label>
											<input required name="excel_file" type="file" accept=".xlsx" class="form-control">											
										</div>
									</div>
									<div class="col-md-12 col-sm-12 mb-4 mt-3">
										<button type="submit" class="btn btn-primary w-100" name="add_staff" id="add_staff" data-toggle="modal">Upload</button>
									</div>
								   </form>
								</div>
							</div>
							</div>
						</div>
						</div>
					</div>
				</div>

				<div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue">Add Student Manually</h4>
							<p class="mb-20"></p>
						</div>
					</div>
					<div class="wizard-content">
						<form class="mt-3" method="post" action="{{url('sso/addStudentManually')}}">
							@csrf
							<section>
								<div class="row">
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label>Full Name :</label>
											<input name="fullname" type="text" class="form-control wizard-required" required="true" autocomplete="off">
										</div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label >Enroll No. :</label>
											<input name="id" type="number" class="form-control" required="true" autocomplete="off">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label>Email Address: <i>Email format(121212.gcit@rub.edu.bt)</i> </label>
											<input name="email" type="email" class="form-control" required="true" autocomplete="off">
										</div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label>Phone Number :</label>
											<input name="phoneNumber" type="number" class="form-control" required="true" autocomplete="off">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-4 col-sm-12">
										<div class="form-group">
											<label>Year :</label>
											<select name="year" class="custom-select form-control" required="true" autocomplete="off">
												<option value="">Select Year</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												
											</select>
										</div>
									</div>
									<div class="col-md-4 col-sm-12">
										<div class="form-group">
											<label>Course :</label>
											<select name="course" class="custom-select form-control" required="true" autocomplete="off">
												<option value="">Select Course</option>
												<option value="BSc.IT">BSc.IT</option>
												<option value="BSc.CS">BSc.CS</option>
												<option value="Blockchain">Blockchain</option>
												<option value="FullStack">FullStack</option>
												<option value="AI">AI</option>
											</select>
										</div>
									</div>
									<div class="col-md-4 col-sm-12">
										<div class="form-group">
											<label>Gender :</label>
											<select name="gender" class="custom-select form-control" required="true" autocomplete="off">
												<option>Select Gender</option>
												<option value="male">Male</option>
												<option value="female">Female</option>
											</select>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12 col-sm-12">
										<div class="form-group">
											<label style="font-size:16px;"><b></b></label>
											<div class="modal-footer justify-content-start">
												<button class="btn btn-primary" name="add_staff" id="add_staff" data-toggle="modal">Add&nbsp;Student</button>
											</div>
										</div>
									</div>
								</div>
							</section>
						</form>
					</div>
				</div>

			</div>
            {{-- @include('sso.includes.footer') --}}
		</div>
	</div>
	
	@include('sso.includes.scripts')

</body>
</html>