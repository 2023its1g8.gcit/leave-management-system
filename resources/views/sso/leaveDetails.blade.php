<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>SLMS</title>

	<!-- {{ asset('vendors/images/apple-touch-icon.png') }} -->
	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('vendors/images/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('vendors/images/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('../vendors/images/favicon-16x16.png') }}">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <style>
        input[type="text"]
        {
            font-size:16px;
            color: #0f0d1b;
            font-family: Verdana, Helvetica;
        }
    
        .btn-outline:hover {
          color: #fff;
          background-color: #524d7d;
          border-color: #524d7d; 
        }
    
        textarea { 
            font-size:16px;
            color: #0f0d1b;
            font-family: Verdana, Helvetica;
        }
    
        textarea.text_area{
            height: 8em;
            font-size:16px;
            color: #0f0d1b;
            font-family: Verdana, Helvetica;
          }
    
        </style>
	<!-- CSS -->

	<link rel="stylesheet" type="text/css" href="{{ asset('vendors/styles/core.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendors/styles/icon-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('src/plugins/jquery-steps/jquery.steps.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('src/plugins/datatables/css/dataTables.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('src/plugins/datatables/css/responsive.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendors/styles/style.css') }}">


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>

	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
</head>

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

	@include('sso.includes.navbar')

	@include('sso.includes.right_sidebar')

	@include('sso.includes.left_sidebar')


	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>LEAVE DETAILS</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
									<li class="breadcrumb-item"><a href="allLeaves">All Leave</a></li>
									<li class="breadcrumb-item active" aria-current="page">Leave Details</li>
								</ol>
							</nav>
						</div>
						
					</div>
				</div>

				<div class="pd-20 card-box mb-30">
					<div class="clearfix">
						<div class="pull-left">
							<h4 class="text-blue h4">Leave Details</h4>
							<p class="mb-20"></p>
						</div>
					</div>

					<form method="post" action="">

						<div class="row">
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Full Name</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="Student">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Enrollment ID</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-success" readonly value="1223432">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Email Address</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-info" readonly value="student@rub.edu.bt">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Gender</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-success" readonly value="Male">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Course</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-success" readonly value="BSc.IT">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Year</b></label>
									<input type="text" name="year" class="selectpicker form-control" data-style="btn-outline-success" readonly value="4">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Contact Number</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="17678204">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Leave Type</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-info" readonly value="Medical">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Applied Date</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-success" readonly value="10th oct">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label style="font-size:16px;"><b>Leave Period</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-info" readonly value="From 10 to 11">
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Number of Days</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-info" readonly value="5">
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 p-0 mb-3">
							<label style="font-size:16px;"><b>Reason: </b></label>
							<textarea class="selectpicker form-control" data-style="btn-outline-info" readonly>I want to go hospital</textarea>
						</div>
						<div class="form-group row">
							<div class="col-md-6 col-sm-12">
							    <div class="form-group">
									<label style="font-size:16px;"><b>Date For SSO's Action</b></label>
                                    <input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="NA">

							    </div>
							</div>
							<div class="col-md-6 col-sm-12">
								<div class="form-group">
									<label style="font-size:16px;"><b>Date For HSA's Action</b></label>
									<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="NA">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label style="font-size:16px;"><b>Leave Status From SSO</b></label>
                                    <input type="text" class="selectpicker form-control" data-style="btn-outline-primary" style="color: green" readonly value="Pending">

								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label style="font-size:16px;"><b>Leave Status From HSA</b></label>
                                    <input type="text" class="selectpicker form-control" data-style="btn-outline-primary" style="color: blue" readonly value="Approved">

								</div>
							</div>
							
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label style="font-size:16px;"><b></b></label>
								<div class="modal-footer justify-content-center">
									<button class="btn btn-primary" id="action_take" data-toggle="modal" data-target="#success-modal">Take&nbsp;Action</button>
								</div>
							</div>
						</div>

						<form name="adminaction" method="post">
							  <div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-body text-center font-18">
											<h4 class="mb-20">Leave take action</h4>
											<select name="status" required class="custom-select form-control">
												<option value="">Choose your option</option>
													  <option value="1">Approved</option>
													  <option value="2">Rejected</option>
											</select>
										</div>
										<div class="modal-footer justify-content-center">
											<input type="submit" class="btn btn-primary" name="update" value="Submit">
										</div>
									</div>
								</div>
							</div>
						  </form>


					</form>
				</div>

			</div>
			
            @include('sso.includes.footer')
		</div>
	</div>
	
	@include('sso.includes.scripts')

</body>
</html>