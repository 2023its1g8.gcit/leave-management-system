{{-- @include('../includes/session.php') --}}
@include('sso.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">	
				Loading...
			</div>
		</div>
	</div> --}}

	@include('sso.includes.navbar')

	@include('sso.includes.right_sidebar')

	@include('sso.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20">
			<div class="card-box pd-20 height-100-p mb-30">
				<div class="row align-items-center">
					<div class="col-md-3 user-icon">
						@if (Auth::user()->profileImg)
						<img style="width: 200px;height:200px;object-fit:cover;object-position:center;border-radius:15px;box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;" src="{{ asset(Auth::user()->profileImg) }}" class="avatar-photo" alt="Profile Image">
						@else
						<div style="width: 200px;height:200px;object-fit:cover;object-position:center;border-radius:15px;box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;" class="profileContainer">
							<div  class="text-uppercase" style="font-size: 60px" >{{ implode(' ', array_map(function($part) { return strtoupper(substr($part, 0, 1)); }, explode(' ', Auth::user()->name))) }}</div>
						</div>
						@endif
					</div>
					<div class="col-md-8">

						<h4 class="font-20 weight-500 mb-10 text-capitalize">
							Welcome back <br> <div class="weight-600 font-30 text-blue">{{ Auth::user()->name }} !</div>
						</h4>
						<p class="font-18 max-width-600">you are in an institution of GCIT.</p>
					</div>
				</div>
			</div>
			<div class="title pb-20">
				<h2 class="h3 mb-0 text-uppercase">Data Information</h2>
			</div>
			<div class="row pb-10">
				<div class="col-xl-6 col-lg-3 col-md-6 mb-20">
					<div class="card-box height-100-p widget-style3">

				
						<div class="d-flex flex-wrap">
							<div class="widget-data">
								<div class="weight-700 font-24 text-dark">{{$leaveCounts['totalStudentCount']}}</div>
								<div class="font-14 text-secondary weight-500">Total Student</div>
							</div>
							<div class="widget-icon">
								<div class="icon" data-color="#00eccf"><i class="icon-copy dw dw-user-2"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-3 col-md-6 mb-20">
					<div class="card-box height-100-p widget-style3">

						<div class="d-flex flex-wrap">
							<div class="widget-data">
								<div class="weight-700 font-24 text-dark">{{$leaveCounts['approved']}}</div>
								<div class="font-14 text-secondary weight-500">Approved Leave</div>
							</div>
							<div class="widget-icon">
								<div class="icon" data-color="#09cc06"><span class="icon-copy fa fa-hourglass"></span></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-3 col-md-6 mb-20">
					<div class="card-box height-100-p widget-style3">

					       

						<div class="d-flex flex-wrap">
							<div class="widget-data">
								<div class="weight-700 font-24 text-dark">{{$leaveCounts['pending']}}</div>
								<div class="font-14 text-secondary weight-500">Pending Leave</div>
							</div>
							<div class="widget-icon">
								<div class="icon"><i class="icon-copy fa fa-hourglass-end" aria-hidden="true"></i></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-3 col-md-6 mb-20">
					<div class="card-box height-100-p widget-style3">

				
						<div class="d-flex flex-wrap">
							<div class="widget-data">
								<div class="weight-700 font-24 text-dark">{{$leaveCounts['rejected']}}</div>
								<div class="font-14 text-secondary weight-500">Rejected Leave</div>
							</div>
							<div class="widget-icon">
								<div class="icon" data-color="#ff5b5b"><i class="icon-copy fa fa-hourglass-o" aria-hidden="true"></i></div>
							</div>
						</div>
					</div>
				</div>
			</div>


			{{-- <div class="card-box mb-30">
				<div class="pd-20">
						<h2 class="text-blue h4">LATEST LEAVE APPLICATIONS</h2>
					</div>
				<div class="pb-20">
					<table class="data-table table stripe hover nowrap">
						<thead>
							<tr>
								<th class="table-plus datatable-nosort">STUDENT NAME</th>
								<th>LEAVE TYPE</th>
								<th>APPLIED DATE</th>
								<th>SSO STATUS</th>
								<th>HSA STATUS</th>
								<th class="datatable-nosort">ACTION</th>
							</tr>
						</thead>
						<tbody>
							<tr>

							

								<td class="table-plus">
									<div class="name-avatar d-flex align-items-center">
										<div class="txt mr-2 flex-shrink-0">
											<b></b>
										</div>
										<div class="txt">
											<div class="weight-600"></div>
										</div>
									</div>
								</td>
								<td></td>
	                            <td></td>
								<td>
	                            </td>
	                           <td>
									

							   </td>
								<td>
									<div class="dropdown">
										<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
											<i class="dw dw-more"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
											<a class="dropdown-item" href=""><i class="dw dw-eye"></i> View</a>
											<a class="dropdown-item" href=""><i class="dw dw-delete-3"></i> Delete</a>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
			   </div>
			</div> --}}

			{{-- @include('sso.includes.footer') --}}

			{{-- <form method="POST" action="{{ route('logout') }}">
				@csrf
				<a href="{{ route('logout') }}" onclick="event.preventDefault();
					this.closest('form').submit();">Logout</a>
			</form> --}}
		</div>
	</div>

	@include('sso.includes.scripts')

</body>
</html>