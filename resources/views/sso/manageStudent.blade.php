@include('sso.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

	@include('sso.includes.navbar')

	@include('sso.includes.right_sidebar')

	@include('sso.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20">
			<div class="page-header">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="title">
							<h4 class="text-uppercase">Manage Student</h4>
						</div>
						<nav aria-label="breadcrumb" role="navigation">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
								<li class="breadcrumb-item active" aria-current="page">Manage Student</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>

			<div class="card-box mb-30">
				<div class="pd-20">
						<h2 class="text-blue h4">ALL STUDENTS</h2>
					</div>
				<div class="pb-20">
					<table class="data-table table stripe hover nowrap">
						<thead>
							<tr>
								<th class="table-plus">FULL NAME</th>
								<th>EMAIL</th>
								<th>COURSE</th>
								<th>YEAR</th>
								<th>CONTACT NO.</th>
								<th class="datatable-nosort">ACTION</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($students as $s)
							<tr>
								<td class="table-plus">
									<div class="name-avatar d-flex align-items-center">
										<div class="avatar mr-2 flex-shrink-0">
											@if ($s->profileImg)
											<img style="width: 40px;height:40px;object-fit:cover;object-position:center;border-radius: 100%;border: 1px solid #000;" src="{{ asset($s->profileImg) }}" class="avatar-photo" alt="Profile Image">
											@else
											<div class="profileContainer">
												<div class="text-uppercase">{{ implode(' ', array_map(function($part) { return strtoupper(substr($part, 0, 1)); }, explode(' ', $s->name))) }}</div>
											</div>
											@endif
								
										</div>
										<div class="txt pl-3">
											<div class="weight-600">{{$s->name}}</div>
										</div>
									</div>
								</td>
								<td>{{$s->email}}</td>
	                            <td>{{$s->course}}</td>
								<td>{{$s->year}}</td>
								<td>{{$s->contact_number}}</td>
								<td>
									<div class="dropdown">
										<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
											<i class="dw dw-more"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
											<a class="dropdown-item" data-toggle="modal" data-target="#editStudent{{$s->id}}" href="#"><i class="dw dw-edit2"></i> Edit</a>
											<a class="dropdown-item" href="" data-toggle="modal" data-target=".DeleteStudent{{$s->id}}"><i class="dw dw-delete-3"></i> Delete</a>
										</div>
									</div>
								</td>
							</tr>
		

							<div class="modal fade DeleteStudent{{$s->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
									<div class="modal-content p-4" style="border-radius: 20px;">
										<div class="modal-header">
											<h5 class="modal-title" id="editStudentLabel">Delete Student: {{$s->name}}</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form action="{{url('/sso/deleteStudent')}}" method="POST">
												@csrf
												Are you sure you want to delete this student <span style="color: red">({{$s->name}})</span> ?
												<input hidden type="number" name="id" value="{{$s->id}}">
												<div class="modal-footer border-0 mt-4">
													<button type="submit" class="btn btn-primary">Yes</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
												  </div>
											</form>
											
										</div>
									</div>
								</div>
							</div>
							<!-- Edit Student Modal -->
							<div class="modal fade" id="editStudent{{$s->id}}" tabindex="-1" role="dialog" aria-labelledby="editStudentLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content p-4" style="border-radius: 20px;">
									<div class="modal-header">
									<h5 class="modal-title" id="editStudentLabel">Edit student: {{$s->name}}</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>
									<div class="modal-body">
										<div class="wizard-content">
											<form class="mt-3" method="post" action="{{url('sso/studentsUpdate')}}">
												@csrf
												<input hidden  type="number" name="id" value="{{$s->id}}">
												<section>
													<div class="row">
														<div class="col-md-6 col-sm-12">
															<div class="form-group">
																<label>Full Name :</label>
																<input name="fullname" type="text" class="form-control wizard-required" value="{{$s->name}}" required="true" autocomplete="off">
															</div>
														</div>
														<div class="col-md-6 col-sm-12">
															<div class="form-group">
																<label>Phone Number :</label>
																<input name="phoneNumber"  value="{{$s->contact_number}}" type="number" class="form-control" required="true" autocomplete="off">
															</div>
														</div>
													</div>
				
													<div class="row">
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Year :</label>
																<select name="year" class="custom-select form-control" required="true" autocomplete="off">
																	<option value="{{$s->year}}">{{$s->year}}</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																</select>
															</div>
														</div>
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Course :</label>
																<select name="course" class="custom-select form-control" required="true" autocomplete="off">
																	<option value="{{$s->course}}">{{$s->course}}</option>
																	<option value="BSc.IT">BSc.IT</option>
																	<option value="BSc.CS">BSc.CS</option>
																	<option value="Blockchain">Blockchain</option>
																	<option value="FullStack">FullStack</option>
																	<option value="AI">AI</option>
																</select>
															</div>
														</div>
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Gender :</label>
																<select name="gender" class="custom-select form-control" required="true" autocomplete="off">
																	<option value="{{$s->gender}}">{{$s->gender}}</option>
																	<option value="male">Male</option>
																	<option value="female">Female</option>
																</select>																
															</div>
														</div>
													</div>
													
													<div class="row">
														<div class="col-md-12 col-sm-12">
															<div class="form-group">
																<label style="font-size:16px;"><b></b></label>
																<div class="modal-footer justify-content-start">
																	<button class="btn w-100 btn-primary" name="add_staff" id="add_staff" data-toggle="modal">Save The Changes</button>
																</div>
															</div>
														</div>
													</div>
												</section>
											</form>
										</div>
									</div>
								</div>
								</div>
							</div>
							@endforeach
						</tbody>
					</table>
			   </div>
			</div>
			{{-- @include('sso.includes.footer') --}}
		</div>
	</div>
	
	@include('sso.includes.scripts')

</body>
</html>