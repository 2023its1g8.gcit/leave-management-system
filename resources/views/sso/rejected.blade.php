@include('sso.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

	@include('sso.includes.navbar')

	@include('sso.includes.right_sidebar')

	@include('sso.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20">
			<div class="page-header">
				<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>REJECTED LEAVES</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
									<li class="breadcrumb-item active" aria-current="page">Rejected Leaves</li>
								</ol>
							</nav>
						</div>
				</div>
			</div>

			<div class="card-box mb-30">
				<div class="pd-20">
						<h2 class="text-blue h4">ALL LEAVE APPLICATIONS</h2>
					</div>
				<div class="pb-20">
					<table class="data-table table stripe hover nowrap">
						<thead>
							<tr>
								<th class="table-plus datatable-nosort">STUDENT NAME</th>
								<th>LEAVE TYPE</th>
								<th>APPLIED DATE</th>
								<th>SSO STATUS</th>
								<th>HSA STATUS</th>
								<th class="datatable-nosort">ACTION</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($leaves as $l)
							<tr>

								<!-- fetch data here --> 

								<td class="table-plus">
									<div class="name-avatar d-flex align-items-center">
										<div class="avatar mr-2 flex-shrink-0">
                                            @if ($l->user->profileImg)
                                            <img style="width:30px;height:30px;object-fit:cover;object-position:center;border-radius:100%;" src="{{ asset($l->user->profileImg) }}" class="avatar-photo" alt="Profile Image">
                                            @else
											<div class="profileContainer">
												<div class="text-uppercase">{{ implode(' ', array_map(function($part) { return strtoupper(substr($part, 0, 1)); }, explode(' ', $l->user->name))) }}</div>
											</div>                                            
											@endif
										</div>
										<div class="txt">
											<div class="weight-600">{{$l->user->name}}</div>
										</div>
									</div>
								</td>
								<td>{{$l->leave_type}}</td>
	                            <td>{{ $l->created_at->format('j M Y') }}</td>
								<td>			
									@if ($l->sso_status === null)
									<span style="color: blue">Pending</span>
									@else
									<span  class="text-capitalize" style="color: {{ $l->sso_status === 'approve' ? 'green' : ($l->sso_status === 'decline' ? 'red' : 'black') }}">
										{{ $l->sso_status }}
									  </span>  
									@endif
								</td>
	                            <td>
									@if ($l->hsa_status === null)
									<span style="color: blue">Pending</span>
									@else
									<span  class="text-capitalize" style="color: {{ $l->hsa_status === 'approve' ? 'green' : ($l->hsa_status === 'decline' ? 'red' : 'black') }}">
										{{ $l->hsa_status }}
									  </span>  
									@endif
								</td>
								<td>
									<div class="dropdown">
										<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
											<i class="dw dw-more"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
											<button type="button" class="btn dropdown-item" data-toggle="modal" data-target="#studentLeaveView{{$l->id}}">
												<i class="dw dw-eye"></i> View
											</button>
											
											<a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Delete</a>
										</div>
									</div>
								</td>
							</tr>
		
						<!--View Leave Modal -->
						<div class="modal fade" id="studentLeaveView{{$l->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="studentLeaveViewLabel" aria-hidden="true">
							<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl" >
							<div class="modal-content py-3 px-5" style="border-radius: 20px">
								<div class="modal-header">
								<h5 class="modal-title" id="studentLeaveViewLabel">{{$l->user->name}}'s Leave</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>
								<div class="modal-body">
								<div>
									<div class="row">
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Full Name</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="{{$l->user->name}}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Enrollment ID</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-success" readonly value="{{$l->user->id}}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Email Address</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-info" readonly value="{{$l->user->email}}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Gender</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-success" readonly value="{{$l->user->gender}}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Course</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-success" readonly value="{{$l->user->course}}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Year</b></label>
												<input type="text" name="year" class="selectpicker form-control" data-style="btn-outline-success" readonly value="{{$l->user->year}}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Contact Number</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="{{$l->user->contact_number}}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Leave Type</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-info" readonly value="{{$l->leave_type}}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Applied Date</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-success" readonly value="{{ $l->created_at->format('j M Y') }}">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label style="font-size:16px;"><b>Leave Period</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-info" readonly value="From {{ \Carbon\Carbon::parse($l->start_date)->format('j M') }} to {{ \Carbon\Carbon::parse($l->end_date)->format('j M') }}">
											</div>
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Number of Days</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-info" readonly value="{{ \Carbon\Carbon::parse($l->start_date)->diffInDays(\Carbon\Carbon::parse($l->end_date)) }}">
											</div>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 p-0 mb-3">
										<label style="font-size:16px;"><b>Reason: </b></label>
										<textarea class="selectpicker form-control" data-style="btn-outline-info" readonly>{{$l->reason}}</textarea>
									</div>
									@if ($l->sso_status !== null)
									<div class="form-group row">
										<div class="col-md-6 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Date For SSO's Action</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="{{ is_null($l->sso_approval_date) ? 'NA' : $l->sso_approval_date}}">

											</div>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="form-group">
												<label style="font-size:16px;"><b>Date For HSA's Action</b></label>
												<input type="text" class="selectpicker form-control" data-style="btn-outline-primary" readonly value="{{ is_null($l->hsa_approval_date) ? 'NA' : $l->hsa_approval_date}}">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label style="font-size:16px;"><b>Leave Status From SSO</b></label>
												<input type="text" class="selectpicker text-capitalize form-control" data-style="btn-outline-primary"  style="color: {{ $l->sso_status === 'approve' ? 'green' : ($l->sso_status === 'decline' ? 'red' : 'black') }}" readonly value="{{$l->sso_status}}">
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label style="font-size:16px;"><b>Leave Status From HSA</b></label>
												<input type="text" class="text-capitalize selectpicker form-control" data-style="btn-outline-primary" style="color: {{ is_null($l->hsa_status) ? 'blue' : ($l->hsa_status === 'approve' ? 'green' : ($l->hsa_status === 'decline' ? 'red' : 'black')) }}" readonly value="{{ is_null($l->hsa_status) ? 'Pending' : $l->hsa_status }}">
											</div>
										</div>
										
									</div>
									@elseif($l->sso_status === null)
									<div class="col-md-12">
										<div class="form-group">
											<label style="font-size:16px;"><b></b></label>
											<div class="modal-footer justify-content-center">
												<button class="btn btn-primary" id="action_take" data-toggle="modal" data-target="#success-modal">Take Action</button>
											</div>
										</div>
									</div>
									{{-- Leave approve or reject --}}
									<form action="{{url('sso/LeaveAproveReject')}}" method="POST">
										@csrf
										<div class="modal fade" style="background:#0000004d" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered" role="document">
												<div class="modal-content">
													<div class="modal-body text-center font-18">
														<h4 class="mb-20">Leave take action</h4>
														<select name="sso_status" required class="custom-select form-control">
															<option>Choose your option</option>
																<option value="approve">Approved</option>
																<option value="decline">Rejected</option>
														</select>
													</div>
													<div class="modal-footer justify-content-center">
														<input type="text" hidden name="leaveID" value="{{$l->id}}">
														<input type="text" hidden name="sso_approval_date" value="{{ date('Y-m-d') }}">
														<input type="submit" class="btn btn-primary" name="update" value="Submit">
													</div>
												</div>
											</div>
										</div>
									</form>
									@endif

								</div>
								</div>
							</div>
							</div>
						</div>

						@endforeach
						</tbody>
					</table>
			   </div>
			</div>

			@include('sso.includes.footer')
		</div>
	</div>
	
	@include('sso.includes.scripts')

</body>
</html>