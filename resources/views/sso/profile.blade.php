@include('sso.includes.header')

<body>
	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../vendors/images/deskapp-logo-svg.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

	@include('sso.includes.navbar')

	@include('sso.includes.right_sidebar')

	@include('sso.includes.left_sidebar')

	<div class="mobile-menu-overlay"></div>

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="title">
								<h4>PROFILE</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
									<li class="breadcrumb-item active" aria-current="page">Profile</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
						<div class="pd-20 card-box height-100-p">

							<div class="profile-photo">
								<a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar"><i class="fa fa-pencil"></i></a>
								@if (Auth::user()->profileImg)
								<img style="width: 150px;height:150px;object-fit:cover;object-position:center;" src="{{ asset(Auth::user()->profileImg) }}" class="avatar-photo" alt="Profile Image">
								@else
								<div style="width: 150px;height:150px;object-fit:cover;object-position:center;" class="profileContainer">
									<div style="font-size: 40px;" class="text-uppercase" style="font-weight: 500;font-size:14px;">{{ implode(' ', array_map(function($part) { return strtoupper(substr($part, 0, 1)); }, explode(' ', Auth::user()->name))) }}</div>
								</div>	
								@endif
									<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="weight-500 col-md-12 pd-5">
													<div class="form-group">
														<form action="{{ route('profile.updateProfileImage') }}" method="POST" enctype="multipart/form-data">
															@csrf
															@method('PUT')
			
															<div class="weight-500 col-md-12 pd-5">
																		<div class="form-group">
																			<div class="custom-file">
																				<input name="profileImg" id="file" type="file" class="custom-file-input" accept="image/*" onchange="validateImage('file')">
																				<label class="custom-file-label" for="file" id="selector">Choose file</label>		
																			</div>
																		</div>
																	</div>
																	<div class="modal-footer">
																		<input type="submit" name="update_image" value="Update" class="btn btn-primary">
																		<button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
																	</div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
							</div>
							<div class="profile-info">
								<h5 class="mb-20 h5 text-blue text-uppercase">Contact Information</h5>
								<ul>
                                    <li class="d-flex">
										Name: <span  class="pl-2 text-capitalize">{{Auth::user()->name}}</span>
								
									</li>
                                    <li class="d-flex">
										Employee Number: <span  class="pl-2">{{Auth::user()->id}}</span>
									
									</li>
									<li class="d-flex">
										Email: <span  class="pl-2">{{Auth::user()->email}}</span>
										
									</li>
									<li class="d-flex">
										Contact Number: <span  class="pl-2">{{Auth::user()->contact_number}}</span>
										
									</li>
									<li class="d-flex">
										Role: <span class="pl-2">Student Service Officer</span>
									
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
                        <div class="card-box height-100-p overflow-hidden">
                            <div class="profile-setting">
                                <form method="POST" action="{{url('/updateProfileDetails')}}">
									@csrf
									<input type="number" hidden name="id" value="{{Auth::user()->id}}">
                                    <div class="profile-edit-list row px-md-5">
                                        <div class="col-md-12">
                                            <h4 class="text-blue h5 mb-20 text-uppercase">Edit Your Personal Setting</h4>
                                        </div>
                    
                                        <div class="weight-500 col-md-12">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input name="name" class="form-control form-control-lg" type="text" required="true" autocomplete="off" value="{{Auth::user()->name}}">
                                            </div>
                                        </div>
                   
                                        <div class="weight-500 col-md-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" class="form-control form-control-lg" type="text" placeholder="" required="true" autocomplete="off" value="{{Auth::user()->email}}">
                                            </div>
                                        </div>
										<div class="weight-500 col-md-12">
                                            <div class="form-group">
                                                <label>Contact Number</label>
                                                <input name="contact_number" class="form-control form-control-lg" type="text" placeholder="" required="true" autocomplete="off" value="{{Auth::user()->contact_number}}">
                                            </div>
                                        </div>
                                        <div class="weight-500 col-md-12">
                                            <div class="form-group">
                                                <label></label>
                                                <div class="modal-footer justify-content-start">
                                                    <button type="submit" class="btn btn-primary" name="new_update" id="new_update" data-toggle="modal">Save & Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                    
				</div>
			</div>
			{{-- @include('sso.includes.footer') --}}
		</div>
	</div>
	
    @include('sso.includes.scripts')

	<script type="text/javascript">
		var loader = function(e) {
			let file = e.target.files;

			let show = "<span>Selected file : </span>" + file[0].name;
			let output = document.getElementById("selector");
			output.innerHTML = show;
			output.classList.add("active");
		};

		let fileInput = document.getElementById("file");
		fileInput.addEventListener("change", loader);
	</script>
	<script type="text/javascript">
		 function validateImage(id) {
		    var formData = new FormData();
		    var file = document.getElementById(id).files[0];
		    formData.append("Filedata", file);
		    var t = file.type.split('/').pop().toLowerCase();
		    if (t != "jpeg" && t != "jpg" && t != "png") {
		        alert('Please select a valid image file');
		        document.getElementById(id).value = '';
		        return false;
		    }
		    if (file.size > 1050000) {
		        alert('Max Upload size is 1MB only');
		        document.getElementById(id).value = '';
		        return false;
		    }

		    return true;
		}
	</script>
</body>
</html>