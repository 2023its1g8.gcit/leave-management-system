<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BackPanel\SsoController;
use App\Http\Controllers\BackPanel\HsaController;
use App\Http\Controllers\BackPanel\StdController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::redirect('/', '/login');

Route::get('/login', function () {
    return view('auth.login')->name('login');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    // profile update
    Route::put('/Profileupdate', [ProfileController::class, 'updateProfile'])->name('profile.updateProfileImage');
    Route::post('/updateProfileDetails', [ProfileController::class, 'updateProfileDetails'])->name('profile.updateProfileDetails');


});

require __DIR__.'/auth.php';

Route::middleware(['auth', 'role:sso'])->group(function () {
    Route::get('sso/dashboard', [SsoController::class, 'dashboard']);
    Route::get('sso/profile', [SsoController::class, 'profile']);
    Route::get('sso/allLeaves', [SsoController::class, 'allLeaves']);
    Route::get('sso/PendingLeaves', [ssoController::class, 'PendingLeaves']);
    Route::get('sso/RejectedLeaves', [ssoController::class, 'RejectedLeaves']);
    Route::get('sso/ApprovedLeaves', [ssoController::class, 'ApprovedLeaves']);
    Route::post('sso/LeaveAproveReject', [SsoController::class, 'LeaveAproveReject']);
    Route::post('sso/importStudentExcel', [SsoController::class, 'importStudentExcel']);

    Route::get('sso/viewL', [SsoController::class, 'viewLeaves']);
    
    Route::post('sso/addStudentManually', [SsoController::class, 'addStudentManually']);
    Route::post('sso/studentsUpdate', [SsoController::class, 'editStudent'])->name('students.update');
    Route::post('sso/deleteStudent', [SsoController::class, 'deleteStudent'])->name('delete.student');

    Route::get('sso/addStd', [SsoController::class, 'addStd']);
    Route::get('sso/mgStd', [SsoController::class, 'mgStd']);
    Route::get('sso/edStd', [SsoController::class, 'edStd']);

    Route::get('sso/changePass', [SsoController::class, 'ChangePass']);
});

Route::middleware(['auth', 'role:hsa'])->group(function () {
    Route::get('hsa/dashboard', [HsaController::class, 'dashboard']);

    Route::get('hsa/profile', [HsaController::class, 'profile']);
    Route::get('hsa/allLeaves', [HsaController::class, 'allLeaves']);
    Route::get('hsa/PendingLeaves', [HsaController::class, 'PendingLeaves']);
    Route::get('hsa/RejectedLeaves', [HsaController::class, 'RejectedLeaves']);
    Route::get('hsa/ApprovedLeaves', [HsaController::class, 'ApprovedLeaves']);

    Route::get('hsa/viewL', [HsaController::class, 'viewLeaves']);
    Route::post('hsa/LeaveAproveReject', [HsaController::class, 'LeaveAproveReject']);

    Route::get('hsa/addSso', [HsaController::class, 'addSso']);
    Route::get('hsa/mgSso', [HsaController::class, 'mgSso']);

    Route::get('hsa/applyLeave', [HsaController::class, 'applyLeave']);
    Route::post('hsa/ApplyStudentLeave', [HsaController::class, 'ApplyStudentLeave']);


    Route::get('hsa/changePass', [HsaController::class, 'ChangePass']);
    // add new sso
    Route::post('hsa/AddNewSSO', [HsaController::class, 'AddNewSSO']);
    Route::post('hsa/editSSO', [HsaController::class, 'editSSO']); 
    Route::post('hsa/deleteSSO', [HsaController::class, 'deleteSSO']);

});

Route::middleware(['auth', 'role:std'])->group(function () {
    Route::get('std/dashboard', [StdController::class, 'dashboard'])->name('stdDashboard');
    Route::get('std/profile', [StdController::class, 'profile'])->name('stdProfile');
    Route::get('std/applyLeave', [StdController::class, 'applyLeave'])->name('stdApplyLeave');
    Route::get('std/leaveHistory', [StdController::class, 'leaveHistory'])->name('stdLeaveHistory');
    Route::get('std/viewLeave/{id}', [StdController::class, 'viewLeave'])->name('stdViewLeave');
    
    Route::get('std/changePass', [StdController::class, 'ChangePass']);

    // post
    Route::post('std/uploadLeave', [StdController::class, 'uploadLeave']);


});