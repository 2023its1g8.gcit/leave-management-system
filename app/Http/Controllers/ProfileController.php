<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Support\Facades\Storage;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->validated());

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        $request->user()->save();

        return Redirect::route('profile.edit')->with('status', 'profile-updated');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }

    public function  updateProfile(Request $request)
        {
            // Validate the incoming request
            $request->validate([
                'profileImg' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            ]);

            // Retrieve the currently logged-in user
            $user = Auth::user();

            // Handle profile picture update
            if ($request->hasFile('profileImg')) {
                // Delete old profile picture if it exists
                if ($user->profileImg) {
                    unlink(public_path($user->profileImg));
                }

                // Move the uploaded file to the public directory
                $file = $request->file('profileImg');
                $fileName = 'profile_' . $user->id . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('profile_pictures'), $fileName);

                // Update user's profile image path in the database
                $user->profileImg = 'profile_pictures/' . $fileName;
                $user->save();

                return redirect()->back()->with('success', 'Profile picture updated successfully');
            }

            // If no file was provided, return with an error message
            return redirect()->back()->with('error', 'No profile picture provided');
        }
        public function updateProfileDetails(Request $request)
        {
            // Validate the input data
            $request->validate([
                'email' => 'required|email',
                'contact_number' => 'required|numeric',
                'name' => 'required',
            ]);
        
            // Get the authenticated user
            $user = Auth::user();
        
            // Get input data
            $email = $request->input('email');
            $contactNumber = $request->input('contact_number');
            $name = $request->input('name');
        
            // Check if the provided email is already used by other users
            $existingEmailUser = User::where('email', $email)->where('id', '!=', $user->id)->first();
            if ($existingEmailUser) {
                return redirect()->back()->with('error', 'Email already in use by another use');

            }
        
            // Check if the provided contact number is already used by other users
            $existingContactUser = User::where('contact_number', $contactNumber)->where('id', '!=', $user->id)->first();
            if ($existingContactUser) {
                return redirect()->back()->with('error', 'Contact number already in use by another user');

            }
        
            // Update the user's profile details
            $user->update([
                'email' => $email,
                'contact_number' => $contactNumber,
                'name' => $name,
            ]);
    
            return redirect()->back()->with('success', 'Profile details updated successfully');

        }
        
        
}


