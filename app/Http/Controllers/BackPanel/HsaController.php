<?php

namespace App\Http\Controllers\BackPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Leave;
use Notification;
use App\Notifications\NewUserPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class HsaController extends Controller
{
    public function dashboard()
    {
        return view('hsa.dashboard');
    }
    // add sso
    public function AddNewSSO(Request $request)
    {
        $role = $request->input('role');
        $email = $request->input('email');
        $name = $request->input('name');
        $employeID = $request->input('employeID');
        $gender = $request->input('gender');
        $phoneNumber = $request->input('phoneNumber');
        $password = Str::random(20);
        $NewUser = [
            'password' => $password,
        ];
        $emailto = $email;
    
        // Check if phone number length is between 8 and 8 characters and starts with '17' or '77'
        $validPhoneNumber = preg_match('/^(17|77)\d{6}$/', $phoneNumber);
    
        // Check if name contains only alphabetic characters
        $validName = preg_match('/^[A-Za-z\s]+$/', $name);
    
        if ($validPhoneNumber && $validName) {
            $existingEmail = User::where('email', $email)->first();
            $existingPhoneNumber = User::where('contact_number', $phoneNumber)->first();
            $existingEmployeeID = User::where('id', $employeID)->first();
    
            if (!$existingEmail && !$existingPhoneNumber && !$existingEmployeeID) {
                $newUserSSO = new User();
                $newUserSSO->id = $employeID;
                $newUserSSO->role = $role;
                $newUserSSO->email = $email;
                $newUserSSO->name = $name;
                $newUserSSO->contact_number = $phoneNumber;
                $newUserSSO->gender = $gender;
                $newUserSSO->password =  $password;
                $newUserSSO->save();
    
                Notification::route('mail', $emailto)->notify(new NewUserPassword($NewUser));
                return redirect('/hsa/mgSso')->with('success', 'Registered successfully!');
            } else {
                $errorMsg = [];
    
                if ($existingEmail) {
                    $errorMsg[] = 'Email is already in use!';
                }
    
                if ($existingPhoneNumber) {
                    $errorMsg[] = 'Phone number is already in use!';
                }
    
                if ($existingEmployeeID) {
                    $errorMsg[] = 'Employee ID is already in use!';
                }
    
                return redirect()->back()->with('error', implode(' ', $errorMsg));
            }
        } else {
            // Validation failed, return specific error messages
            $errorMsg = [];
    
            if (!$validPhoneNumber) {
                $errorMsg[] = 'Invalid phone number format! It should be bhutanese number.';
            }
    
            if (!$validName) {
                $errorMsg[] = 'Invalid name format! It should contain only alphabetic characters.';
            }
    
            return redirect()->back()->with('error', implode(' ', $errorMsg));
        }
    }
    

    public function editSSO(Request $request)
    {
        $id = $request->input('id');
        $role = $request->input('role');
        $email = $request->input('email');
        $name = $request->input('name');
        $employeID = $request->input('employeID');
        $gender = $request->input('gender');
        $phoneNumber = $request->input('phoneNumber');
    
        // Check if phone number length is between 8 and 8 characters and starts with '17' or '77'
        $validPhoneNumber = preg_match('/^(17|77)\d{6}$/', $phoneNumber);
    
        // Check if name contains only letters and spaces
        $validName = preg_match('/^[A-Za-z\s]+$/', $name);
    
        // Check if the new email is unique (excluding the current user)
        $existingEmail = User::where('email', $email)->where('id', '!=', $id)->first();
    
        // Check if the new phone number is unique (excluding the current user)
        $existingPhoneNumber = User::where('contact_number', $phoneNumber)->where('id', '!=', $id)->first();
    
        // Check if the new employee ID is unique (excluding the current user)
        $existingEmployeeID = User::where('id', $employeID)->where('id', '!=', $id)->first();
    
        if ($validPhoneNumber && $validName) {
            try {
                $existingUser = User::find($id);
    
                if ($existingUser) {
                    if (!$existingEmail && !$existingPhoneNumber && !$existingEmployeeID) {
                        $existingUser->role = $role;
                        $existingUser->email = $email;
                        $existingUser->name = $name;
                        $existingUser->id = $employeID;
                        $existingUser->gender = $gender;
                        $existingUser->contact_number = $phoneNumber;
                        $existingUser->save();
    
                        return redirect('/hsa/mgSso')->with('success', 'User updated successfully!');
                    } else {
                        $errorMsg = [];
    
                        if ($existingEmail) {
                            $errorMsg[] = 'Email is already in use!';
                        }
    
                        if ($existingPhoneNumber) {
                            $errorMsg[] = 'Phone number is already in use!';
                        }
    
                        if ($existingEmployeeID) {
                            $errorMsg[] = 'Employee ID is already in use!';
                        }
    
                        return redirect()->back()->with('error', implode(' ', $errorMsg));
                    }
                } else {
                    return redirect()->back()->with('error', 'User not found!');
                }
            } catch (QueryException $e) {
                // Catch duplicate entry exception
                $errorCode = $e->errorInfo[1];
    
                if ($errorCode == 1062) {
                    return redirect()->back()->with('error', 'Error: Duplicate entry for the Employee ID. Please choose a different Employee ID.');
                } else {
                    // Handle other exceptions or rethrow the exception
                    throw $e;
                }
            }
        } else {
            // Validation failed, return specific error messages
            $errorMsg = [];
    
            if (!$validPhoneNumber) {
                $errorMsg[] = 'Invalid phone number format! It should be 8 digits starting with 17 or 77.';
            }
    
            if (!$validName) {
                $errorMsg[] = 'Invalid name format! It should contain only letters and spaces.';
            }
    
            if ($existingEmployeeID) {
                $errorMsg[] = 'Employee ID is already in use!';
            }
    
            return redirect()->back()->with('error', implode(' ', $errorMsg));
        }
    }

    
    public function deleteSSO(Request $request)
    {
        $id = $request->input('user_id');
        // Fetch the user by ID
        $user = User::find($id);

        // Check if the user exists
        if (!$user) {
            return redirect()->back()->with('error', "SSO not found!");
        }

        // Delete the user
        $user->delete();

        return redirect('/hsa/mgSso')->with('success', 'SSO deleted successfully!');
    }

    public function ApplyStudentLeave(Request $request)
    {
        $leave_type = $request->input('leave_type');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $reason = $request->input('reason');
        $user_id = $request->input('enrollment_number');

        // Check if the user with the specified ID exists
        $userCheck = User::where('id', $user_id)->exists();

        if (!$userCheck) {
            return redirect()->back()->with('error', 'Student not found. Leave application cannot be submitted.');
        }

        $leaveApply = new Leave();
        $leaveApply->leave_type = $leave_type;
        $leaveApply->start_date = $start_date;
        $leaveApply->end_date = $end_date;
        $leaveApply->user_id = $user_id;
        $leaveApply->reason = $reason;

        if ($leaveApply->save()) {
            return redirect()->back()->with('success', 'Leave application submitted successfully!');
        } else {
            return redirect()->back()->with('error', 'Failed to submit leave application. Please try again.');
        }
    }


    public function profile()
    {
        return view('hsa.profile');
    }
    public function allLeaves()
    {
        $LeaveLists = Leave::where('sso_status', 'approve')->get();
        return view('hsa.leaves',compact('LeaveLists'));
    }
    public function PendingLeaves()
    {
        $LeaveLists = Leave::whereNull('hsa_status')
                    ->Where('sso_status','approve')
                    ->get();
        return view('hsa.pedening',compact('LeaveLists'));
    }
    public function RejectedLeaves()
    {
        $LeaveLists = Leave::where('hsa_status', 'decline')
                    ->Where('sso_status','approve')
                    ->get();
        return view('hsa.rejected',compact('LeaveLists'));
    }
    public function ApprovedLeaves()
    {
        $LeaveLists = Leave::where('hsa_status', 'approve')
                    ->Where('sso_status','approve')
                    ->get();
        return view('hsa.approved',compact('LeaveLists'));
    }
    public function LeaveAproveReject(Request $request)
    {
        $request->validate([
            'leaveID' => 'required|exists:leave,id',
            'hsa_status' => 'required|in:approve,decline',
            'hsa_approval_date' => 'nullable|date'
        ]);
    
        $leaveID = $request->input('leaveID');
        $leave = Leave::find($leaveID);
    
        if ($leave) {
            $hsaStatus = $request->input('hsa_status');
            $hsa_approval_date = $request->input('hsa_approval_date');
    
            try {
           
                $leave->update(['hsa_status' => $hsaStatus, 'hsa_approval_date' => $hsa_approval_date]);
                $message = 'You have ' . ($hsaStatus === 'approve' ? 'approved' : 'declined') . ' the student leave successfully.';
                return redirect()->back()->with('message', $message);
            } catch (\Exception $e) {
                return redirect()->back()->with('error', 'Failed to update leave.');
            }
        } else {
            return redirect()->back()->with('error', 'Leave not found.');
        }
    }

    public function viewLeaves()
    {
        return view('hsa.leaveDetails');
    }
    public function addSso()
    {
        return view('hsa.addSsO');
    }
    public function mgSso()
    {
        $ssoUser = User::where('role','sso')->get();
        return view('hsa.manageSsO',compact('ssoUser'));
    }
    public function edSso($id)
    {
        // Fetch the user by ID
        $user = User::find($id);
        return view('hsa.editSso',compact('user'));
    }
    public function applyLeave()
    {
        return view('hsa.applyLeave');
    }
    public function ChangePass()
    {
        return view('hsa.changePass');
    }
}
