<?php

namespace App\Http\Controllers\BackPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Leave;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use Notification;
use App\Notifications\NewUserPassword;
use Illuminate\Validation\Rule;
use App\Imports\StudentDataImport;
use Maatwebsite\Excel\Facades\Excel;



class SsoController extends Controller
{
    public function dashboard()
    {
        $rejectedLeavesCount = Leave::where('sso_status', 'decline')->count();
        $pendingLeavesCount = Leave::whereNull('sso_status')->count();
        $approvedLeavesCount = Leave::where('sso_status', 'approve')->count();
        $totalStudentCount = User::where('role','std')->count();
        
        $leaveCounts = [
            'rejected' => $rejectedLeavesCount,
            'pending' => $pendingLeavesCount,
            'approved' => $approvedLeavesCount,
            'totalStudentCount'=>$totalStudentCount
        ];

        return view('sso.dashboard',compact('leaveCounts'));
    }
    public function profile()
    {
        return view('sso.profile');
    }
    public function allLeaves()
    {
        $leaves = Leave::orderBy('created_at', 'desc')->get();
        return view('sso.leaves',compact('leaves'));
    }
    public function PendingLeaves()
    {
        $leaves = Leave::whereNull('sso_status')
                    ->get();
        return view('sso.pending',compact('leaves'));
    }
    public function RejectedLeaves()
    {
        $leaves = Leave::where('sso_status', 'decline')
                    ->get();
        return view('sso.rejected',compact('leaves'));
    }
    public function ApprovedLeaves()
    {
        $leaves = Leave::where('sso_status', 'approve')
                    ->get();
        return view('sso.approved',compact('leaves'));
    }
    public function LeaveAproveReject(Request $request)
    {
        $request->validate([
            'leaveID' => 'required|exists:leave,id',
            'sso_status' => 'required|in:approve,decline',
            'sso_approval_date' => 'nullable|date'
        ]);
    
        $leaveID = $request->input('leaveID');
        $leave = Leave::find($leaveID);
    
        if ($leave) {
            $SsoStatus = $request->input('sso_status');
            $sso_approval_date = $request->input('sso_approval_date');
    
            try {
                // Update the sso_status and sso_approval_date
                if($SsoStatus === 'decline'){
                    $leave->update(['sso_status'=>$SsoStatus,'sso_approval_date'=>$sso_approval_date,'hsa_status'=>'decline']);
                }else{
                    $leave->update(['sso_status' => $SsoStatus, 'sso_approval_date' => $sso_approval_date]);
                }
                $message = 'You have ' . ($SsoStatus === 'approve' ? 'approved' : 'declined') . ' the student leave successfully.';
                return redirect()->back()->with('message', $message);
            } catch (\Exception $e) {
                return redirect()->back()->with('error', 'Failed to update leave.');
            }
        } else {
            return redirect()->back()->with('error', 'Leave not found.');
        }
    }
    
    public function viewLeaves()
    {
        return view('sso.leaveDetails');
    }
    public function addStd()
    {
        return view('sso.addStudent');
    }

    public function addStudentManually(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'fullname' => [
                    'required',
                    'string',
                    'regex:/^[a-zA-Z\s]+$/',
                ],
                'id' => [
                    'required',
                    'string',
                    'unique:users,id',
                    'min:8',
                    'max:8',
                ],
                'email' => [
                    'required',
                    'email',
                    'unique:users,email',
                    function ($attribute, $value, $fail) use ($request) {
                        $enrollmentNo = $request->input('id');
                        if ($value !== $enrollmentNo . '.gcit@rub.edu.bt') {
                            $fail('The email format is incorrect. It should be ' . $enrollmentNo . '.gcit@rub.edu.bt');
                        }
                    },
                ],
                'phoneNumber' => [
                    'required',
                    'string',
                    'unique:users,contact_number',
                    'regex:/^(17|77)\d{6}$/',
                ],
                'year' => 'required|integer',
                'course' => 'required|string',
                'gender' => 'required|in:male,female',
            ], [
                'fullname.regex' => 'The full name must only contain alphabetic characters.',
                'id.unique' => 'The enrollment No is already taken.',
                'id.min' => 'The enrollment No must be at least 8 characters.',
                'id.max' => 'The enrollment No must not exceed 8 characters.',
                'email.unique' => 'The email address is already taken.',
                'phoneNumber.unique' => 'The contact number is already taken or does not meet the required format.',
                'phoneNumber.regex' => 'The contact number is invalid!',
            ]);            
            // Extract variables from validated data
            $fullname = $validatedData['fullname'];
            $enrollmentNo = $validatedData['id'];
            $email = $validatedData['email'];    
            $phoneNumber = $validatedData['phoneNumber'];
            $year = $validatedData['year'];
            $course = $validatedData['course'];
            $gender = $validatedData['gender'];
            $role = 'std';
    
            // Generate a strong random password
            $password = Str::random(20);
            $NewUser = [
                'password' => $password,
            ];
            $emailto = $email;

            Notification::route('mail', $emailto)->notify(new NewUserPassword($NewUser));
            // Create a new user and save it to the database
            $newUser = User::create([
                'name' => $fullname,
                'id' => $enrollmentNo,
                'email' => $email,
                'password' => $password,
                'role' => $role,
                'contact_number' => strval($phoneNumber),
                'year' => $year,
                'course' => $course,
                'gender' => $gender,
            ]);

            return redirect('sso/mgStd')->with('success', 'Student added successfully.');
        }catch (ValidationException $e) {
            // Fetch validation errors
            $validationErrors = $e->errors();
        
            // Check for specific validation errors and display custom messages
            if (isset($validationErrors['id']) && in_array('users.id', $validationErrors['id'])) {
                return redirect()->back()->with('error', 'The enrollment ID is already taken.')->withInput();
            } elseif (isset($validationErrors['email']) && in_array('users.email', $validationErrors['email'])) {
                return redirect()->back()->with('error', 'The email address is already taken.')->withInput();
            } elseif (isset($validationErrors['phoneNumber']) && in_array('users.contact_number', $validationErrors['phoneNumber'])) {
                return redirect()->back()->with('error', 'The contact number is already taken or does not meet the required format.')->withInput();
            } else {
                // Redirect back with general validation errors
                return redirect()->back()->withErrors($validationErrors)->withInput();
            }
        }
        
    }

    public function editStudent(Request $request)
    {
        try {
            // Update the student based on the provided ID
            $user = User::find($request->input('id'));
    
            if (!$user) {
                return redirect()->back()->with('error', 'Student not found.');
            }
    
            // Validate the new contact number
            $request->validate([
                'phoneNumber' => [
                    'required',
                    'string',
                    'min:8',
                    'max:8',
                    'regex:/^(17|77)\d{6}$/',
                ],
            ]);
    
            // Check if the new phone number is unique for other users
            $existingUser = User::where('contact_number', $request->input('phoneNumber'))
                                ->where('id', '!=', $user->id)
                                ->first();
    
            if ($existingUser) {
                // If the phone number is taken by another user, show an error only if it's a new phone number
                if ($request->input('phoneNumber') !== $user->contact_number) {
                    return redirect()->back()->with('error', 'The phone number has already been taken by another user.')->withInput();
                }
            }
    
            // Update the user data
            $user->name = $request->input('fullname');
            $user->contact_number = $request->input('phoneNumber');
            $user->year = $request->input('year');
            $user->course = $request->input('course');
            $user->gender = $request->input('gender');
    
            // Save the updated user
            $user->save();
    
            return redirect('sso/mgStd')->with('success', 'Student updated successfully.');
        } catch (\Exception $e) {
            // Handle exceptions, e.g., database errors
            return redirect()->back()->with('error', 'An error occurred while updating the student: ' . $e->getMessage())->withInput();
        }
    }
    
    
    public function deleteStudent(Request $request)
    {
        try {
            // Get the student ID from the input
            $studentId = $request->input('id');

            // Find the student based on the provided ID
            $user = User::find($studentId);

            if (!$user) {
                return redirect()->back()->with('error', 'Student not found.');
            }

            // Delete the student
            $user->delete();

            return redirect('sso/mgStd')->with('success', 'Student deleted successfully.');
        } catch (\Exception $e) {
            // Handle exceptions, e.g., database errors
            return redirect()->back()->with('error', 'An error occurred while deleting the student: ' . $e->getMessage());
        }
    }
    
    
    
    public function mgStd()
    {
        $students = User::where('role', 'std')->get();
        return view('sso.manageStudent',compact('students'));
    }
    public function edStd()
    {
        return view('sso.editStudent');
    }
    public function ChangePass()
    {
        return view('sso.changePass');
    }

    public function importStudentExcel(Request $request)
    {
        // Set a 1-hour time limit for this script
        set_time_limit(3600);
    
        if ($request->hasFile('excel_file')) {
            $file = $request->file('excel_file');
            $import = new StudentDataImport();
    
            try {
                Excel::import($import, $file);
    
                $updatedCount = $import->getUpdatedCount();
                $newCount = $import->getNewCount();
                $skippedCount = $import->getSkippedCount();
    
                $message = "Updated $updatedCount student(s), added $newCount new student(s), and skipped $skippedCount invalid record(s).";
    
                return redirect('sso/mgStd')->with('success', $message);
            } catch (ValidationException $e) {
                // Handle validation errors
                $errorMessages = $e->validator->errors()->get('error');
                return redirect('sso/addStd')->with('error', $errorMessages[0]);
            } catch (\Exception $e) {
                // Handle other exceptions
                dd($e); // You might want to log or handle this differently
                return redirect('sso/addStd')->with('error', 'An unknown error occurred during the import.');
            }
        } else {
            return "No file uploaded.";
        }
    }
}