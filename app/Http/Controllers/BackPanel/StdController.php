<?php

namespace App\Http\Controllers\BackPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Leave;


class StdController extends Controller
{
    public function dashboard()
    {
        $user_id = auth()->user()->id;
        $leaveRecord = Leave::where('user_id', $user_id)->get();

        // leave counts
        $leaveAppliedCount = Leave::where('user_id', $user_id)->count();
        $pendingLeaveCount = Leave::where('user_id', $user_id)
            ->where(function ($query) {
                $query->whereNull('hsa_status')
                    ->orWhereNull('sso_status');
            })
            ->count();
        $approveLeaveCount = Leave::where('user_id', $user_id)
            ->where(function ($query) {
                $query->where('hsa_status','approve')
                    ->Where('sso_status','approve');
            })
            ->count();
        $declineLeaveCount = Leave::where('user_id', $user_id)
            ->where(function ($query) {
                $query->where('hsa_status','decline')
                    ->orWhere('sso_status','decline');
            })
            ->count();

        return view('std.dashboard',compact('leaveRecord','leaveAppliedCount','pendingLeaveCount','approveLeaveCount','declineLeaveCount'));
    }
    public function profile()
    {
        return view('std.profile');
    }
    public function applyLeave()
    {
        return view('std.applyLeave');
    }
    public function leaveHistory()
    {
        $user_id = auth()->user()->id;
        $leaveRecord = Leave::where('user_id', $user_id)
            ->orderBy('created_at', 'desc')
            ->get();
    
        return view('std.leaveHistory',compact('leaveRecord'));
    }
    public function viewLeave($id)
    {
        $viewLeave = Leave::find($id);
        return view('std.viewLeave',compact('viewLeave'));
    }
    public function ChangePass()
    {
        return view('std.changePass');
    }

    public function uploadLeave(Request $request)
    {
        $leave_type = $request->input('leave_type');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $reason = $request->input('reason');
        $user_id = auth()->user()->id;
    
        $leaveApply = new Leave();
        $leaveApply->leave_type = $leave_type;
        $leaveApply->start_date = $start_date;
        $leaveApply->end_date = $end_date;
        $leaveApply->user_id = $user_id;
        $leaveApply->reason = $reason;
    
        if ($leaveApply->save()) {
            return redirect('std/leaveHistory')->with('success', 'Leave application submitted successfully!');
        } else {
            return redirect()->back()->with('error', 'Failed to submit leave application. Please try again.');
        }
    }
        
}


