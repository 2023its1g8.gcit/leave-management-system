<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
 public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                // Check user role and redirect accordingly
                $role = $request->user()->role;

                if ($role === 'sso') {
                    return redirect()->intended(RouteServiceProvider::SSO);
                } elseif ($role === 'hsa') {
                    return redirect()->intended(RouteServiceProvider::HSA);
                } elseif ($role === 'std') {
                    return redirect()->intended(RouteServiceProvider::Student);
                } else {
                    // Add a default redirect if the role is not matched
                    return redirect(RouteServiceProvider::HOME);
                }
            }
        }

        return $next($request);
    }
}
