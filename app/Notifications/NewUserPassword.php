<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewUserPassword extends Notification
{
    use Queueable;
    private $NewUser;

    /**
     * Create a new notification instance.
     */
    public function __construct($NewUser)
    {
       $this->NewUser = $NewUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                    ->greeting('New user password for GCIT SLMS')
                    ->line('Your Password: '.$this->NewUser['password'])
                    ->action('Login', url('http://127.0.0.1:8000/login'))
                    ->line('Please do not share your password with anyone. You can log in with the provided password and your email address, and then update the password, if required.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
