<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $table = 'leave'; // Specify the table name

    protected $fillable = [
        'start_date',
        'end_date',
        'hsa_status',
        'sso_status',
        'hsa_approval_date',
        'sso_approval_date',
    ];
    use HasFactory;
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}

