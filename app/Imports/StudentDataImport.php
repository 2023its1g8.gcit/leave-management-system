<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Imports\StudentDataImport;
use Maatwebsite\Excel\Facades\Excel;
use Notification;
use App\Notifications\NewUserPassword;

class StudentDataImport implements ToModel, WithHeadingRow
{
    private $updatedCount = 0;
    private $newCount = 0;
    private $skippedCount = 0;
    // Define an array of required column names.
    private $requiredColumns = [
        'student_id',
        'student_name',
        'year',
        'course_name',
        'gender',
        'phone_number',
    ];
    
    public function model(array $row)
    {
        // Check for null values in the row.
        if (
            $row['student_id'] === null ||
            $row['student_name'] === null ||
            $row['year'] === null ||
            $row['course_name'] === null ||
            $row['gender'] === null ||
            $row['phone_number'] === null
        ) {
            // If any of the required columns have null values, skip this row.
            $this->skippedCount++; // Increment the skipped count.
            return null;
        }

        $existingStudent = User::where('id', $row['student_id'])->first(); // Use Eloquent or your database query method to find the student.

        $gender = strtolower($row['gender']); // Convert gender to lowercase for case-insensitivity.

        if ($gender === 'f') {
            $gender = 'female';
        } elseif ($gender === 'm') {
            $gender = 'male';
        } else {
            // Handle other cases if needed.
            // For example, you can set a default gender or handle invalid gender values.
            // $gender = 'Unknown';
        }

        if ($existingStudent) {
            // Update the existing student.
            $existingStudent->name = $row['student_name'];
            $existingStudent->year = (int)$row['year'];
            $existingStudent->course = $row['course_name'];
            $existingStudent->gender = $gender; // Use the modified gender value.
            $existingStudent->contact_number = (int)$row['phone_number'];
            $existingStudent->email =  strval($row['student_id']).'.gcit@rub.edu.bt';


            $this->updatedCount++;
            return $existingStudent; // Return the updated student object.
        } else {
            // If the student doesn't exist, create a new student.
            // Generate a strong random password
            $password = Str::random(20);
            $newPassword = $password;
  
            return new User([
                'id' => (int)$row['student_id'],
                'name' => $row['student_name'],
                'year' => (int)$row['year'],
                'course' => $row['course_name'],
                'gender' => $gender, // Use the modified gender value.
                'contact_number' => (int)$row['phone_number'],
                'email' =>  strval($row['student_id']).'.gcit@rub.edu.bt',
                'password' => $password,
            ]);
            $NewUser = [
                'password' => $newPassword,
            ];
            $emailto = strval($row['student_id']).'.gcit@rub.edu.bt';

            Notification::route('mail', $emailto)->notify(new NewUserPassword($NewUser));
            $this->newCount++;
        }
    }
    public function getUpdatedCount()
    {
        return $this->updatedCount;
    }

    public function getNewCount()
    {
        return $this->newCount;
    }

    public function getSkippedCount()
    {
        return $this->skippedCount;
    }
}