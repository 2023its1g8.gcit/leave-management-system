<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'id' => '111',
                'name' => 'Sso',
                'email' => 'sso@gmail.com',
                'role' => 'sso',
                'status' => 'active',
                'password' => bcrypt('password'),
                'gender' => 'male',
                'contact_number' => '17678204'
            ],
            [
                'id' => '222',
                'name' => 'Hsa',
                'email' => 'hsa@gmail.com',
                'role' => 'hsa',
                'status' => 'active',
                'password' => bcrypt('password'),
                'gender' => 'female',
                'contact_number' => '17678204'
            ],
            [
                'id' => '333',
                'name' => 'std',
                'email' => 'std@gmail.com',
                'role' => 'std',
                'status' => 'active',
                'password' => bcrypt('password'),
                'gender' => 'male',
                'contact_number' => '17678204',
                // 'year' => '4',
                // 'course' => 'BSc.IT'
            ]
        ]);
    }
}
